extends Area2D

var TYPE
var SIDE
export (int) var SPEED

var velocity = Vector2()
var direction = Vector2()
var grid

const TOP = Vector2(0, -1)
const RIGHT = Vector2(1, 0)
const LEFT = Vector2(-1, 0)
const DOWN = Vector2(0, 1)

var is_moving = false
var target_pos = Vector2()
var target_direction = Vector2()

func _ready(): # Main trucho
	grid = get_parent()

func _process(delta):
	# Movimiento
	direction = Vector2()
	velocity = Vector2()

	if Input.is_action_pressed("ui_up"):
		direction = TOP
	elif Input.is_action_pressed("ui_right"):
		direction = RIGHT
	elif Input.is_action_pressed("ui_left"):
		direction = LEFT
	elif Input.is_action_pressed("ui_down"):
		direction = DOWN

	if not is_moving and direction != Vector2():
		target_direction = direction
		
		if grid.is_cell_vacant(position, target_direction):
			flip()
			target_pos = grid.update_child_pos(self)
			is_moving = true
	
	elif is_moving:
		# Evitamos que velocity esté ligada al framerate
		velocity = SPEED * target_direction * delta

		var distance_to_target = Vector2(abs(target_pos.x - position.x), 
										 abs(target_pos.y - position.y))

		if abs(velocity.x) > distance_to_target.x:
			velocity.x = distance_to_target.x * target_direction.x
			is_moving = false

		if abs(velocity.y) > distance_to_target.y:
			velocity.y = distance_to_target.y * target_direction.y
			is_moving = false

	position += velocity


func flip():
	if $AnimatedSprite.frame == 0:
		$AnimatedSprite.frame = 1
		SIDE = 1
	else:
		$AnimatedSprite.frame = 0
		SIDE = 0

	
	
	
	
	
	

