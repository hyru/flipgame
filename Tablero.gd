extends TileMap

# VARIABLES
var tile_size = get_cell_size()
var half_tile_size = tile_size / 2

enum TIPO_FICHA {FICHA1, FICHA2}
enum LADO_FICHA {LADO1, LADO2}

var grid_size = Vector2(4,4)
var amount_of_cells = grid_size.x * grid_size.y
var grid = []

# FUNCIONES
func _ready():
	# Dibuja el tablero centrado
	var window_size = OS.get_window_size()
	
	# El tamaño del tile es cada celda por su tamaño individual
	position = Vector2((window_size.x - (tile_size.x * grid_size.x)) / 2, 
					   (window_size.y - (tile_size.y * grid_size.x)) / 2)
	
	# Inicializa el tablero
	for x in range(grid_size.x):
		grid.append([])

		for y in range(grid_size.y):
			grid[x].append([null, null])

	# Inicializa la primer ficha
	var Ficha = get_node("Ficha")
	Ficha.TYPE = 0
	Ficha.SIDE = 0
	var start_pos = update_child_pos(Ficha)
	Ficha.position = start_pos


func is_cell_vacant(pos, direction):
	var grid_pos = world_to_map(pos) + direction
	
	if (grid_pos.x < grid_size.x and grid_pos.x >= 0 and
		grid_pos.y < grid_size.y and grid_pos.y >= 0):
		return grid[grid_pos.x][grid_pos.y] == [null, null]

	else:
		return false

func update_child_pos(child_node):

	""" 
	Move a child to a new position in the grid Array
	Returns the new target world position of the child
	"""

	var grid_pos = world_to_map(child_node.position)
	grid[grid_pos.y][grid_pos.x] = [null, null]

	var new_grid_pos = grid_pos + child_node.direction
	
	# Tipo de ficha en cierta casilla
	if child_node.TYPE == 0 and child_node.SIDE == 0:
		grid[new_grid_pos.y][new_grid_pos.x] = [FICHA1, LADO1]
	elif child_node.TYPE == 0 and child_node.SIDE == 1:
		grid[new_grid_pos.y][new_grid_pos.x] = [FICHA1, LADO2]
	elif child_node.TYPE == 1 and child_node.SIDE == 0:
		grid[new_grid_pos.y][new_grid_pos.x] = [FICHA2, LADO1]
	elif child_node.TYPE == 1 and child_node.SIDE == 1:
		grid[new_grid_pos.y][new_grid_pos.x] = [FICHA2, LADO2]

	# Testing
	print(new_grid_pos)
	for x in range(len(grid)):
		prints(grid[x])

	var target_position = map_to_world(new_grid_pos) + half_tile_size	
	return target_position
